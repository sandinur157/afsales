<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing_address extends Model
{
    protected $guarded = ['id'];
}
