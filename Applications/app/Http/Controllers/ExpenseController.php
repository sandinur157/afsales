<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;

class ExpenseController extends Controller
{
    public function index()
    {
        return view('expenses.index');
    }

    
    public function listData() 
    {
        $expenses = Expense::latest()->get();
        $no = 0;
        $data = array();

        foreach ($expenses as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->date;
            $row[] = $list->name;
            $row[] = format_uang($list->price);
            $row[] = $list->amount;
            $row[] = format_uang($list->price * $list->amount);
            $row[] = $list->note;
            $row[] = '
                    <a href="#" onclick="edit('. $list->id .')" class="btn btn-link"><i class="fas fa-pencil-alt"></i></a>
                    <a href="#" onclick="_delete('. $list->id .')" class="btn btn-link text-danger"><i class="fas fa-trash-alt"></i></a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function edit(Expense $expense)
    {
        echo json_encode($expense);
    }
    
    public function store(Request $request)
    {
        $price = explode(',', $request->price);
        $price = implode('', $price);
        $amount = explode(',', $request->amount);
        $amount = implode('', $amount);

        $expenses = Expense::create([
            'date' => $request->date,
            'name' => $request->name,
            'price' => $price,
            'amount' => $amount,
            'subtotal' => $price * $amount,
            'note' => $request->note
        ]);

        return response()->json([
            'message' => 'Pengeluaran berhasil ditambahkan.'
        ]);
    }

    public function update(Request $request, Expense $expense)
    {
        $price = explode(',', $request->price);
        $price = implode('', $price);
        $amount = explode(',', $request->amount);
        $amount = implode('', $amount);

        $expense->update([
            'date' => $request->date,
            'name' => $request->name,
            'price' => $price,
            'amount' => $amount,
            'subtotal' => $price * $amount,
            'note' => $request->note
        ]);

        return response()->json([
            'message' => 'Pengeluaran berhasil diubah.'
        ]);
    }

    
    public function destroy(Expense $expense)
    {
        $expense->delete();
        return response()->json([
            'message' => 'Data pengeluaran berhasil dihapus.'
        ]);
    }
}
