<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function index()
    {
        return view('customers.index');
    }

    public function listData()
    {
        $customers = Customer::latest()->get();
        $no = 0;
        $data = array();

        foreach ($customers as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->name;
            $row[] = $list->customer_category;
            $row[] = $list->phone;
            $row[] = $list->address;
            $row[] = '
                    <a href="#" onclick="edit('. $list->id .')" class="btn btn-link text-primary"><i class="fas fa-pencil-alt"></i></a>
                    <a href="#" onclick="_delete('. $list->id .')" class="btn btn-link text-danger"><i class="fas fa-trash-alt"></i></a>
                    <a href="#" onclick="detail('. $list->id .')" class="btn btn-link text-secondary"><i class="fas fa-eye"></i></a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function store(Request $request)
    {
        $customer = Customer::create([
            'name' => $request->name,
            'customer_category' => $request->customer_category,
            'province' => $request->province,
            'district' => $request->district,
            'sub_district' => $request->sub_district,
            'postal_code' => $request->postal_code,
            'phone' => $request->phone,
            'email' => $request->email,
            'other_contact' => $request->other_contact,
            'address' => $request->address,
        ]);

        return response()->json([
            'message' => 'Customer berhasil ditambahkan.'
        ]);
    }

    public function edit(Customer $customer)
    {
        echo json_encode($customer);
    }

    public function update(Request $request, Customer $customer)
    {
        $customer->update([
            'name' => $request->name,
            'customer_category' => $request->customer_category,
            'province' => $request->province,
            'district' => $request->district,
            'sub_district' => $request->sub_district,
            'postal_code' => $request->postal_code,
            'phone' => $request->phone,
            'email' => $request->email,
            'other_contact' => $request->other_contact,
            'address' => $request->address,
        ]);

        return response()->json([
            'message' => 'Customer berhasil diubah.'
        ]);
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();
        return response()->json([
            'message' => 'Data customer berhasil dihapus.'
        ]);
    }
}
