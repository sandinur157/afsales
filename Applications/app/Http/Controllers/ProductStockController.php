<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_stock;
use App\Product;

class ProductStockController extends Controller
{
    public function index()
    {
        $products = Product::latest()->get();
        return view('product_stocks.index', compact('products'));
    }

    public function listData()
    {
        $product_stocks = Product_stock::with('product')->latest()->get();
        $no = 0;
        $data = array();

        foreach ($product_stocks as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '-';// $list->date_changed;
            $row[] = $list->product->name;
            $row[] = $list->amount;
            $row[] = $list->description;
            $row[] = '
                    <a href="#" onclick="edit('. $list->id .')" class="btn btn-link"><i class="fas fa-pencil-alt"></i></a>
                    <a href="#" onclick="_delete('. $list->id .')" class="btn btn-link text-danger"><i class="fas fa-trash-alt"></i></a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function edit($id)
    {
        $product_stock = Product_stock::find($id);
        echo json_encode($product_stock);
    }

    public function store(Request $request)
    {
        $product_stock = Product_stock::create([
            // 'date_changed' => $request->date_changed,
            'product_id' => $request->product_id,
            'amount' => $request->amount,
            'description' => $request->description
        ]);

        return response()->json([
            'message' => 'Stok baru berhasil ditambahkan.'
        ]);
    }

    public function update(Request $request, $id)
    {
        $product_stock = Product_stock::find($id);
        $product_stock->update([
            // 'date_changed' => $request->date_changed,
            'product_id' => $request->product_id,
            'amount' => $request->amount,
            'description' => $request->description
        ]);

        return response()->json([
            'message' => 'Stok berhasil diubah.'
        ]);
    }

    public function destroy($id)
    {
        $product_stock = Product_stock::find($id);
        $product_stock->delete();
        return response()->json([
            'message' => 'Data stok berhasil dihapus.'
        ]);
    }
}
