<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shipping_address;

use Validator;
use Auth;

class ApiAddressShippingController extends Controller
{
   public function __construct()
	{
		return auth()->shouldUse('reseller');
	}

    public function show()
    {
        $user = Auth::user()->id;
        $address = Shipping_address::where('id_reseller', $user)->first();

        return response()->json(compact('address'));
    }

    public function update(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'country' => 'required',
            'street_address' => 'required',
            'city' => 'required',
            'province' => 'required',
            'postal_code' => 'required'
        ]);

        // validation
        if ($validator->fails()) return response()->json($validator->errors(), 422);

        $user = Auth::user()->id;
        $address = Shipping_address::where('id_reseller', $user)->first();

        $address->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_name' => $request->company_name,
            'country' => $request->country,
            'street_address' => $request->street_address,
            'apartement' => $request->apartement,
            'city' => $request->city,
            'province' => $request->province,
            'postal_code' => $request->postal_code
        ]);

        return response()->json([
            'message' => 'Alamat pengiriman berhasil diperbaharui.'
        ], 201);
    }
}
