<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use App\Category;
use App\Stock_management;
use App\Customer;
use App\User;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::latest()->get();
    	return view('orders.index', compact('orders'));
    }

    public function show($id)
    {
    	# code...
    }

    public function create()
    {
    	return view('orders.create');
    }

    public function store(Request $request)
    {
    	# code...
    }

    public function update(Order $order,  Request $request)
    {
    	# code...
    }

    public function destroy(Order $order)
    {
    	# code...
    }

    public function canceled()
    {
        # code...
    }
}
