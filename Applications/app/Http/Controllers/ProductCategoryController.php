<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_category;

class ProductCategoryController extends Controller
{
    public function index()
    {
        $categories = Product_category::with('products')->latest()->get();
    	return view('product_categories.index', compact('categories'));
    }

    public function edit($id)
    {
        $category = Product_category::find($id);
        echo json_encode($category);
    }

    public function store(Request $request)
    {
    	$this->validate(request(), [
            'name' => 'required|string|min:4|max:25|unique:product_categories',
            'description' => 'nullable|string'
        ]);

        try {
            $category = Product_category::create(request()->all());
            return back()->with('message', [
                'title' => 'Kategori berhasil ditambahkan.'
            ]);

        } catch (Exception $e) {
            return back()->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(), [
            'name' => 'required|string|min:4|max:25',
            'description' => 'nullable|string'
        ]);

        try {
            $category = Product_category::find($id)->update(request()->all());
            return redirect()->route('product_category.index')->with('message', [
                'title' => 'Kategori berhasil diubah.'
            ]);
        } catch (Exception $e) {
            return redirect()->route('product_category.index')->with('message', [
                'title' => $e->getMessage()
            ]);
        }
    }

    public function destroy($id)
    {
    	$category = Product_category::find($id)->delete();
        return back()->with('message', [
            'title' => 'Kategori berhasil dihapus.'
        ]);
    }
}
