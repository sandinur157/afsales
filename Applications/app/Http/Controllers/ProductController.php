<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Product_category;
use App\Product_stock;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('product_category')->latest()->get();        
        $product_categories = Product_category::latest()->get();
    	return view('products.index', compact('products', 'product_categories'));
    }

    public function show($slug)
    {
        $product_categories = Product_category::latest()->get();
        return view('products.show', compact('product_categories'));
    }

    public function create()
    {
        $product_categories = Product_category::latest()->get();
    	return view('products.create', compact('product_categories'));
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $product_categories = Product_category::latest()->get();
        return view('products.edit', compact('product', 'product_categories'));	
    }

    public function store(Request $request)
    {
    	# code...
    }

    public function update(Product $product,  Request $request)
    {
    	# code...
    }

    public function destroy(Product $product)
    {
    	# code...
    }

    public function history_variant($varian_id)
    {
        return view('products.history');
    }
}
