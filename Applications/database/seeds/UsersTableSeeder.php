<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Administrator',
                'username' => 'admin',
                'email' => 'admin@afsales.com',
                'password' => bcrypt('123'),
                'photo' => 'user.png',
                'role_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );

        User::create(
            [
                'name' => 'User',
                'username' => 'user',
                'email' => 'user@afsales.com',
                'password' => bcrypt('123'),
                'photo' => 'user.png',
                'role_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );
    }
}
