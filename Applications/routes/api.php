<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['api']], function () {

	// AUTH
	Route::post('/reseller/register', 'Api\ApiAuthController@register');
	Route::post('/reseller/login', 'Api\ApiAuthController@login');

	Route::group(['middleware' => ['jwt.auth']], function () {

		// PRODUCTS
		Route::get('/product', 'Api\ApiProductController@index');
		Route::get('/product/{id}', 'Api\ApiProductController@show');

		// BILLING ADDRESS
		Route::get('/address/billing', 'Api\ApiAddressBillingController@show');
		Route::post('/address/billing', 'Api\ApiAddressBillingController@update');

		// SHIPPING ADDRESS
		Route::get('/address/shipping', 'Api\ApiAddressShippingController@show');
		Route::post('/address/shipping', 'Api\ApiAddressShippingController@update');
	});

});