@extends('layouts.master')

@section('title', 'Stok Produk')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Produk</a></li>
    <li class="breadcrumb-item active">Stok</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-12">
        @card
            @slot('title')
                <button class="btn btn-primary" onclick="add()"><i class="fas fa-plus-circle"></i> Tambah</button>
            @endslot

            @table
                @slot('thead')
                    <th width="5%">#</th>
                    <th>Tanggal</th>
                    <th>Produk</th>
                    <th>Stok</th>
                    <th>Deskripsi</th>
                    <th width="15%">Aksi</th>
                @endslot
            @endtable
        @endcard
    </div>
</div>
@include('product_stocks.form')
@endsection

@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
    let id, table, save_method;

    jQuery(() => {

        $('.datepicker').datepicker({
            'format': 'dd/mm/yyyy',
            'autoclose': true
        })

        table = $('.table').DataTable({
            'processing' : true,
            'autoWidth' : false,
            'ajax' : {
                'url' : '{{ url('/product/stock/listdata') }}',
                'type' : 'GET'
            },
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups Data tidak ditemukan!."
            }
        })
    })

    $(function() {
        $('#modal-form form').validator().on('submit', function(e) {
            if(!e.preventDefault()) {
                id = $('#id').val();
                
                if(save_method == 'add') url = '{{ route("product_stock.store") }}';
                else url = '{{ url('product/stock') }}/' + id;

                $.ajax({
                    url : url,
                    type : 'POST',
                    data : $('#modal-form form').serialize(),
                    success : function(data) {
                        $('#modal-form').modal('hide');
                        table.ajax.reload();
                        _swall(data.message);
                    }
                })
                return false;
            }
        })
    })

    function add() {
        save_method = 'add';
        $('.needs-validation').removeClass('was-validated')
        $('#modal-form').modal('show');
        $('#modal-form .modal-title').html('Tambah Stok Produk')
        $('input[name=_method]').val('POST');
        $('#modal-form form')[0].reset();
    }

    function edit(id) {
        save_method = 'edit';
        $('.needs-validation').removeClass('was-validated')
        $('input[name=_method]').val('PUT');
        $('#modal-form form')[0].reset();

        $.ajax({
            url : '{{ url('product/stock') }}/' + id + '/edit',
            type : 'GET',
            dataType : 'JSON',
            success : function(data) {
                $('#modal-form').modal('show');
                $('#modal-form .modal-title').html('Edit Stok Produk')

                $('#id').val(data.id);
                $('#date_changed').val(data.date_changed);
                $('#product_id').val(data.product_id);
                $('#amount').val(data.amount);
            }, 
            error : function() {
                alert('Tidak dapat menampilkan data');
            }
        })
    }
 
    function _delete(id) {
        if(confirm('Apakah yakin data akan dihapus?')) {
            $.ajax({
                url : '{{ url('product/stock') }}/' + id,
                type : 'POST',
                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data) {
                    table.ajax.reload();
                    _swall(data.message);
                },
                error : function(data) {
                    alert('Tidak dapat menghapus data!');
                }
            })
        }
    }
</script>
@endpush