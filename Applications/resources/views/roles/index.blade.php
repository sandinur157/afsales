@extends('layouts.master')

@section('title', 'Manajemen Role')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('setting.user') }}">User</a></li>
    <li class="breadcrumb-item active">Manajemen Role</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-4">
        @card
            @slot('card_id', 'card_form')
            @slot('title')
                <h5 class="card-title">Tambah</h5>
                <div class="card-tools">
                    <a href="#" class="btn btn-link text-secondary" onclick="add()" title="Tambah Baru"><i class="fas fa-plus-circle"></i></a>
                </div>
            @endslot

            <form role="form" action="{{ route('role.store') }}" method="post" class="needs-validation" novalidate>
                @csrf @method('post')
                <input type="hidden" name="id">
                <div class="form-group">
                    <label for="name">Role</label>
                    <input type="text" name="name" class="form-control" id="name" required>
                    <div class="invalid-feedback">
                        Masukan Role yang ingin ditambahkan.
                    </div>
                </div>
                <button class="btn btn-primary">Simpan</button>
            </form>
        @endcard
    </div>

    <div class="col-md-8">
        @card
            @slot('title')
                <h5 class="card-title">List Role</h5>
            @endslot
            
            @table
                @slot('thead')
                    <tr>
                        <th>#</th>
                        <th width="50%">Role</th>
                        <th>Created At</th>
                        <th>Aksi</th>
                    </tr>
                @endslot
                
                @php $no = 1; @endphp
                @foreach ($roles as $role)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ date('d/m/Y', strtotime($role->created_at)) }}</td>
                    <td>
                        <a href="#" onclick="edit('{{ $role->id }}')" class="btn btn-link btn-sm"><i class="fa fa-pencil-alt"></i></a>
                        <form action="{{ route('role.destroy', $role->id) }}" method="post" class="d-inline-block">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-link btn-sm text-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            @endtable        
        @endcard
    </div>
</div>
@include('components.sweet')
@endsection


@push('scripts')
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
    let table = $('.table').DataTable({
        "autoWidth" : false,
        "language": {
          "emptyTable": "Ups data masih kosong!.",
          "zeroRecords": "Ups data tidak ditemukan!."
        }
    })

    function edit(id) {
        $('#card_form .card-title').text('Edit Role');
        $('#card_form form').attr('action', '{{ url('/role') }}' +'/'+ id)[0].reset();
        $('input[name=_method]').val('put');

        $.ajax({
            url : '{{ url('/role') }}' +'/'+ id + '/edit',
            type : 'get',
            dataType : 'json',
            success : function(data) {
                $('input[name=id]').val(data.id);
                $('input[name=name]').val(data.name).focus();
            }
        })
    }

    function add() {
        $('#card_form .card-title').text('Tambah');
        $('#card_form form').attr('action', '{{ url('/role') }}')[0].reset();
        $('input[name=_method]').val('post');
        $('input[name=name]').focus();
    }
</script>
@endpush