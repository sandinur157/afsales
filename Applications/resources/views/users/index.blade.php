@extends('layouts.master')

@section('title', 'Manajemen User')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Manajemen User</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-12">
        @card
            @slot('title')
                <a href="#" class="btn btn-primary btn-sm" onclick="add()"><i class="fa fa-plus-circle"></i> Tambah</a>
            @endslot

            @table
                @slot('thead')
	                <th width="30">No</th>
	                <th>Nama User</th>
	                <th>Username</th>
	                <th>Email</th>
	                <th width="100">Aksi</th>
		        @endslot

                <tbody></tbody>
            @endtable
        @endcard
    </div>
</div>

@include('users.form')
@endsection


@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
	let id, table, table1, save_method;
	$(function() {
		table = $('.table').DataTable({
			'processing' : true,
			'autoWidth' : false,
			'ajax' : {
				'url' : '{{ url('user/listdata') }}',
				'type' : 'GET'
			},
			"language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups Data tidak ditemukan!."
            }
		});
	})

	$(function() {
		$('#modal-form form').validator().on('submit', function(e) {
			if(!e.preventDefault()) {
				id = $('#id').val();
				
				if(save_method == 'add') url = '{{ route("user.store") }}';
				else url = '{{ url('/user') }}/' + id;

				$.ajax({
					url : url,
					type : 'POST',
					data : $('#modal-form form').serialize(),
					success : function(data) {
						$('#modal-form').modal('hide');
						table.ajax.reload();

						_swall(data.message);
					}
				})

				return false;
			}
		})
	})

	function add() {
		save_method = 'add';
		$('.needs-validation').removeClass('was-validated')
        $("#modal-form").modal({
            backdrop: 'static',
            keyboard: false
        });
		$('#modal-form').modal('show');
		$('input[name=_method]').val('POST');
		$('#modal-form form')[0].reset();
		$('.modal-title').text('Tambah User');
		$('#password, #password1').attr('required', true);
	}

	function edit(id) {
		save_method = 'edit';
		$('.needs-validation').removeClass('was-validated')
        $("#modal-form").modal({
            backdrop: 'static',
            keyboard: false
        });
		$('input[name=_method]').val('PATCH');
		$('#modal-form form')[0].reset();

		$.ajax({
			url : '{{ url('/user') }}/' + id + '/edit',
			type : 'GET',
			dataType : 'JSON',
			success : function(data) {
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit User');

				$('#id').val(data.id);
				$('#name').val(data.name);
				$('#username').val(data.username);
				$('#email').val(data.email);
				$('#password, #password1').removeAttr('required');
			}, 
			error : function() {
				alert('Tidak dapat menampilkan data');
			}
		})
	}

	function _delete(id) {
        if(confirm('Apakah yakin data akan dihapus?')) {
            $.ajax({
                url : '{{ url('/user') }}/' + id,
                type : 'POST',
                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data) {
                    table.ajax.reload();
                    
                    _swall(data.message);
                },
                error : function(data) {
                    alert('Tidak dapat menghapus data!');
                }
            })
        }
    }
</script>
@endpush
