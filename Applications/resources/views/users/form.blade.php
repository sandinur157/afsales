@modal
    @slot('title', '')
    
    <form method="post" data-toggle="validator">
        @csrf @method('post')
        <input type="hidden" name="id" id="id">

		<div class="form-group">
			<label for="name" class="col-form-label">Nama User</label>
			<div class="col-md-9">
				<input type="text" name="name" id="name" class="form-control" required>
				<span class="help-block with-errors text-danger"></span>
			</div>
		</div>

		<div class="form-group">
			<label for="username" class="col-form-label">Username</label>
			<div class="col-md-9">
				<input type="text" name="username" id="username" class="form-control" required>
				<span class="help-block with-errors text-danger"></span>
			</div>
		</div>

		<div class="form-group">
			<label for="email" class="col-form-label">Email</label>
			<div class="col-md-9">
				<input type="email" name="email" id="email" class="form-control" required>
				<span class="help-block with-errors text-danger"></span>
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-form-label">Password</label>
			<div class="col-md-9">
				<input type="password" name="password" id="password" class="form-control" minlength="6" required>
				<span class="help-block with-errors text-danger"></span>
			</div>
		</div>

		<div class="form-group">
			<label for="password1" class="col-form-label">Konfirmasi Password</label>
			<div class="col-md-9">
				<input type="password" name="password1" id="password1" class="form-control" minlength="6" required data-match="#password">
				<span class="help-block with-errors text-danger"></span>
			</div>
		</div>

        <div class="footer float-right">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endmodal

@push('scripts')
    <script>
        $('.form-group').addClass('row')
        $('.col-form-label').addClass('col-md-3 font-weight-normal')
        $('.form-control').addClass('form-control-sm')
    </script>
@endpush
