@extends('layouts.master')

@section('title', 'Produk')

@push('css')
    <style>
        .form-control.fc_md, .btn_md { 
            padding: 0.5rem 1rem;
            font-size: 1rem;
            line-height: 1;
        }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ url('/products') }}">Produk</a></li>
    <li class="breadcrumb-item active">Histori</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-lg-12">
        @card
            
            <div class="row">
                <div class="col-md-4">
                    <p class="text-muted text-sm">HISTORI STOK</p>
                    <h1>Celana 
                        <small class="bg-light text-sm rounded border p-1">L, Blue Dark</small>
                    </h1>
                </div>

                <div class="col-md-8">
                    <form class="form-inline mt-lg-5 mt-md-5 float-right">
                        <input type="text" class="form-control fc_md mx-sm-2 my-2 my-md-0 my-lg-0 datepicker" name="from_date" id="from_date" placeholder="Dari Tanggal">
                        <input type="text" class="form-control fc_md mx-sm-2 my-2 my-md-0 my-lg-0 datepicker" name="to_date" id="to_date" placeholder="Sampai Tanggal">
                        <button class="btn btn-default px-3 btn-md"><i class="fas fa-search"></i></button>
                    </form>
                </div>

                <div class="col-md-12 table-responsive mt-3">
                    <table class="table table-sm table-bordered">
                        <thead class="thead-light">
                            <th class="text-center" width="3%">No</th>
                            <th>Tanggal Pesan</th>
                            <th>Keterangan</th>
                            <th class="text-center">Masuk</th>
                            <th class="text-center">Keluar</th>
                            <th class="text-center">Sisa Stok</th>
                        </thead>

                        <tbody>
                            @for ($i = 0; $i < 3; $i++)
                                <tr>
                                    <td class="text-center">{{ $i+1 }}</td>
                                    <td>
                                        Sabtu, 26 Oktober 2019 
                                        <span class="d-block text-muted text-sm">Diinputkan 1 jam yang lalu.</span>
                                    </td>
                                    <td>Stok Awal</td>
                                    <td class="text-success font-weight-bold text-center">20</td>
                                    <td class="text-danger font-weight-bold text-center">-</td>
                                    <td class="text-center font-weight-bold">20</td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
            
            @slot('footer')
                <a href="{{ route('product.index') }}" class="btn btn-md btn-orange">Kembali</a>
            @endslot
        @endcard
    </div>
</div>
@include('components.sweet')
@endsection


@push('scripts')
<script>
    $('.datepicker').datepicker()

    $('.table').DataTable({
        paginate: false,
        searching: false,
        bInfo: false,
    })
</script>
@endpush