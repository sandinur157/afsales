@modal
    @slot('title', '')
    
    <form method="post" class="needs-validation" novalidate>
        @csrf @method('post')
        <input type="hidden" name="id" id="id">

        <div class="form-group">
            <div class="col-md-4">
            	<label for="customer_category" class="col-form-label">Kategori</label>
                <select name="customer_category" id="customer_category" class="custom-select custom-select-sm" required>
                	<option disabled>-- Isi Kategori Customer --</option>
                	<option value="pelanggan">Pelanggan</option>
                	<option value="reseller">Reseller</option>
                	<option value="dropshipper">Dropshipper</option>
                </select>
            </div>

            <div class="col-md-8">
            	<label for="name" class="col-form-label">Nama Customer</label>
                <input type="text" name="name" id="name" class="form-control" required>
                <div class="invalid-feedback">
                    Isi nama customer.
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
            	<label for="province" class="col-form-label">Provinsi</label>
                <select name="province" id="province" class="custom-select custom-select-sm">
                	<option disabled>-- Pilih Provinsi --</option>
                </select>
            </div>
            <div class="col-md-4">
            	<label for="district" class="col-form-label">Kabupaten</label>
                <select name="district" id="district" class="custom-select custom-select-sm">
                	<option disabled>-- Isi Pilih Kabupaten --</option>
                </select>
            </div>
            <div class="col-md-4">
            	<label for="sub_district" class="col-form-label">Kecamatan</label>
                <select name="sub_district" id="sub_district" class="custom-select custom-select-sm">
                	<option disabled>-- Isi Pilih Kecamatan --</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-3">
            	<label for="postal_code" class="col-form-label">Kode Pos</label>
                <input type="number" name="postal_code" id="postal_code" class="form-control" required>
                <div class="invalid-feedback">
                    Isi kode pos.
                </div>
            </div>
        
	        <div class="col-md-3">
	        	<label for="phone" class="col-form-label">HP / No telpon</label>
	            <input type="number" name="phone" id="phone" class="form-control" required>
                <div class="invalid-feedback">
                    Isi HP / No telpon.
                </div>
	        </div>

            <div class="col-md-3">
                <label for="email" class="col-form-label">Email</label>
                <input type="email" name="email" id="email" class="form-control" required>
                <div class="invalid-feedback">
                    Isi alamat email valid.
                </div>
            </div>
        
            <div class="col-md-3">
                <label for="other_contact" class="col-form-label">Kontak Lain</label>
                <input type="text" name="other_contact" id="other_contact" class="form-control">
                <div class="valid-feedback">
                    Kontak lain boleh kosong.
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="address" class="col-form-label">Alamat</label>
                <textarea name="address" id="address" class="form-control"></textarea>
                <div class="valid-feedback">
                    Alamat boleh kosong.
                </div>
            </div>
        </div>

        <div class="footer float-right">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endmodal

@push('scripts')
    <script>
        $('.form-group').addClass('row')
        $('.col-form-label').addClass('font-weight-normal')
    </script>
@endpush
