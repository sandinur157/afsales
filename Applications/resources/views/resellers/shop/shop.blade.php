@extends('resellers.partials.layouts.index')

@section('title', 'Order')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Toko</h1>
        </div>
        <div class="row">
            <div class="col-6 col-md-3 col-lg-3">
                <article class="article article-style-c">
                    <a href="">
                        <div class="article-header">
                            <div class="article-image" style="background-image: url('https://smartkids.id/wp-content/uploads/2019/12/IMG-20191202-WA0041-300x300.jpg');"></div>
                        </div>
                        <div class="article-details">
                            <div class="article-title">
                                <h2 class="text-center">
                                    <a href="#">AZALEA BUY 1 GET 1</a>
                                </h2>
                            </div>
                        </div>
                    </a>
                </article>
            </div>

            <div class="col-6 col-md-3 col-lg-3">
                <article class="article article-style-c">
                    <a href="">
                        <div class="article-header">
                            <div class="article-image" style="background-image: url('https://smartkids.id/wp-content/uploads/2019/12/IMG-20191202-WA0041-300x300.jpg');"></div>
                        </div>
                        <div class="article-details">
                            <div class="article-title">
                                <h2 class="text-center">
                                    <a href="#">AZALEA BUY 1 GET 1</a>
                                </h2>
                                <p>
                                    Rp2,349,000.00
                                </p>
                            </div>
                        </div>
                    </a>
                </article>
            </div>

            <div class="col-6 col-md-3 col-lg-3">
                <article class="article article-style-c">
                    <a href="">
                        <div class="article-header">
                            <div class="article-image" style="background-image: url('https://smartkids.id/wp-content/uploads/2019/12/IMG-20191202-WA0041-300x300.jpg');"></div>
                        </div>
                        <div class="article-details">
                            <div class="article-title">
                                <h2 class="text-center">
                                    <a href="#">AZALEA BUY 1 GET 1</a>
                                </h2>
                                <span>
                                    Rp2,550,000.00 Rp2,349,000.00
                                </span>
                            </div>
                        </div>
                    </a>
                </article>
            </div>


            <div class="col-6 col-md-3 col-lg-3">
                    <article class="article article-style-c">
                        <a href="">
                            <div class="article-header">
                                <div class="article-image" style="background-image: url('https://smartkids.id/wp-content/uploads/2019/12/IMG-20191202-WA0041-300x300.jpg');"></div>
                            </div>
                            <div class="article-details">
                                <div class="article-title">
                                    <h2 class="text-center">
                                        <a href="#">AZALEA BUY 1 GET 1</a>
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </article>
                </div>
    
                <div class="col-6 col-md-3 col-lg-3">
                    <article class="article article-style-c">
                        <a href="">
                            <div class="article-header">
                                <div class="article-image" style="background-image: url('https://smartkids.id/wp-content/uploads/2019/12/IMG-20191202-WA0041-300x300.jpg');"></div>
                            </div>
                            <div class="article-details">
                                <div class="article-title">
                                    <h2 class="text-center">
                                        <a href="#">AZALEA BUY 1 GET 1</a>
                                    </h2>
                                    <p>
                                        Rp2,349,000.00
                                    </p>
                                </div>
                            </div>
                        </a>
                    </article>
                </div>
    
                <div class="col-6 col-md-3 col-lg-3">
                    <article class="article article-style-c">
                        <a href="">
                            <div class="article-header">
                                <div class="article-image" style="background-image: url('https://smartkids.id/wp-content/uploads/2019/12/IMG-20191202-WA0041-300x300.jpg');"></div>
                            </div>
                            <div class="article-details">
                                <div class="article-title">
                                    <h2 class="text-center">
                                        <a href="#">AZALEA BUY 1 GET 1</a>
                                    </h2>
                                    <span>
                                        Rp2,550,000.00 Rp2,349,000.00
                                    </span>
                                </div>
                            </div>
                        </a>
                    </article>
                </div>
        </div>
    </section>
@endsection