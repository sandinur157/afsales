@extends('layouts.master')

@section('title', 'Kategori Produk')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Produk</a></li>
    <li class="breadcrumb-item active">Kategori</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-4">
        @card
            @slot('card_id')
                card_form
            @endslot

            @slot('title')
                <h5 class="card-title">Tambah</h5>

                <div class="card-tools">
                    <a href="#" class="btn btn-link text-secondary" onclick="add()" title="Tambah Baru"><i class="fas fa-plus-circle"></i></a>
                </div>
            @endslot
            
            <form action="{{ route('product_category.store') }}" method="post" class="needs-validation" novalidate>
                @csrf @method('post')
                <input type="hidden" name="id">
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" name="name" id="name" class="form-control" minlength="4" required autofocus>
                    <div class="invalid-feedback">
                        Isi kategori dengan benar (min 4 digit).
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="description" rows="3" class="form-control">{{ old('description') }}</textarea>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </form>
        @endcard
    </div>

    <div class="col-md-8">
        @card
            @slot('title')
                <h5 class="card-title">Daftar Kategori</h5>
            @endslot

            @table
                @slot('thead')
                    <th>#</th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th width="40%">Deskripsi</th>
                    <th width="15%">Aksi</th>
                @endslot

                @foreach ($categories as $key => $category)
                <tr>
                    <td>{{ $key +1 }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{{ !empty($category->products) ? $category->products->count() : 0 }}</td>
                    <td>{{ $category->description }}</td>
                    <td>
                        <a href="#" onclick="edit('{{ $category->id }}')" class="btn btn-link btn-sm"><i class="fa fa-pencil-alt"></i></a>
                        <form method="post" action="{{ route('product_category.destroy', $category->id) }}" class="d-inline-block">
                            @csrf @method('delete')
                            <button type="submit" class="btn btn-link btn-sm text-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            @endtable
        @endcard
    </div>
</div>
@include('components.sweet')
@endsection

@push('scripts')
<script src="{{ asset('/js/validate.js') }}"></script>
<script>

    let table = $('.table').DataTable({
        'autoWidth' : false,
        "language": {
          "emptyTable": "Ups data masih kosong!.",
          "zeroRecords": "Ups data tidak ditemukan!."
        }
    })

    function edit(id) {
        $('#card_form .card-title').text('Edit Kategori');
        $('#card_form form').attr('action', '{{ url('/product/category') }}' +'/'+ id)[0].reset();
        $('input[name=_method]').val('put');

        $.ajax({
            url : '{{ url('/product/category') }}' +'/'+ id + '/edit',
            type : 'get',
            dataType : 'json',
            success : function(data) {
                $('input[name=id]').val(data.id);
                $('input[name=name]').val(data.name).focus();
                $('[name=description]').val(data.description);
            }
        })
    }

    function add() {
        $('#card_form .card-title').text('Tambah');
        $('#card_form form').attr('action', '{{ url('/product/category') }}')[0].reset();
        $('input[name=_method]').val('post');
        $('input[name=name]').focus();
    }
</script>
@endpush